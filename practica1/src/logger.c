#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv)
{
	int fd;
	char salida = 'o';
	char buffer[60];

	if(mkfifo("/tmp/com", 0666))
		perror("Error al crear el fifo");	

	fd = open("/tmp/com", O_RDONLY);
	while(1)//Cambiar a esperar sin bloquear
	{		
		if (read(fd, buffer, sizeof(char) * 60) == -1)
		{
			perror("Error al leer");
			break;
		}
		//printf("Leído \n");
		printf("%s \n", buffer);
		if (buffer[0] == salida)
			break;
	}
	close(fd);
	unlink("/tmp/com");
	return 0;
}
