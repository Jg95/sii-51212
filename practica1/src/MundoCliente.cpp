//////////////////////////////////////////////////////////////////////
//                                                                  //
//                 Autor: Javier García López                       //
//                                                                  //
//////////////////////////////////////////////////////////////////////
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo(): jugador1(1), jugador2(2), timer_max(1.0f), max_esferas(10), tiempo_bot2(10.0f), nombre_pipe_coord("/tmp/coordenadas"), 		nombre_pipe_teclas("/tmp/teclas")
{
	Init();
}

CMundo::~CMundo()
{
	printf("Destructor\n");
	for (int i = 0; i < lista_esferas.size(); i++)
		delete lista_esferas[i];
	close(fdCoord);
	unlink(nombre_pipe_coord);
	close(fdTeclas);
	unlink(nombre_pipe_teclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Cliente Jugador1: %d",jugador1.puntos);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",jugador2.puntos);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for (int i = 0; i < lista_esferas.size(); i++)
		lista_esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	//Leer pipe coordenadas
	if (read(fdCoord, buffCoord, sizeof(buffCoord)) == -1)
		perror("Error al leer");
	sscanf(buffCoord,"%d %d", &num_esferas, &esfera_borrada);
	if (esfera_borrada != -1)
	{
		delete lista_esferas[esfera_borrada];
		lista_esferas.erase(lista_esferas.begin() + esfera_borrada);
	}
	if (lista_esferas.size() < num_esferas)
		lista_esferas.push_back(new Esfera);
	for (int i = 0; i < num_esferas; i++)
	{
		if (read(fdCoord, buffCoord, sizeof(buffCoord)) == -1)
			perror("Error al leer");
		sscanf(buffCoord,"%f %f %f", &lista_esferas[i]->centro.x, &lista_esferas[i]->centro.y, &lista_esferas[i]->radio);
	}
	if (read(fdCoord, buffCoord, sizeof(buffCoord)) == -1)
		perror("Error al leer");
	sscanf(buffCoord,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d", 
		&jugador1.x1, &jugador1.x2, &jugador1.y1, &jugador1.y2,
		&jugador1.paralizante.centro.x, &jugador1.paralizante.centro.y,
		&jugador1.reductor.centro.x, &jugador1.reductor.centro.y,
		&jugador2.x1, &jugador2.x2, &jugador2.y1, &jugador2.y2, 
		&jugador2.paralizante.centro.x, &jugador2.paralizante.centro.y,
		&jugador2.reductor.centro.x, &jugador2.reductor.centro.y,
		&jugador1.puntos, &jugador2.puntos);

	//Terminar juego
	if (jugador1.puntos == 3 || jugador2.puntos == 3)
	{
		printf("Fin del juego\n");
		terminarJuego();
	}

	//Logica bot 1
	for (int i = 0; i < lista_esferas.size(); i++)
		if (jugador1.Distancia(lista_esferas[i]->centro, NULL) < dist)
				n = i;

	if (n >= lista_esferas.size())
		n = 0;
	esferaCrit = *lista_esferas[n];
	dist = jugador1.Distancia(esferaCrit.centro, NULL);

	pdatos->esfera = esferaCrit;
	pdatos->raqueta = jugador1; 
	if (pdatos->accion == 1)
		OnKeyboardDown('w', 0, 0);
	if (pdatos->accion == -1)
		OnKeyboardDown('s', 0, 0);
	
	//Temporizador bot 2
	if (temporizador >= tiempo_bot2)
		bot2 = true;
	else
		bot2 = false;
	
	temporizador += 0.025f;
	
	//Logica bot 2
	if (bot2)
	{
		printf("bot2 funcionando\n");
		for (int i = 0; i < lista_esferas.size(); i++)
			if (jugador2.Distancia(lista_esferas[i]->centro, NULL) < dist2)
				n2 = i;

		if (n2 >= lista_esferas.size())
			n2 = 0;
		esferaCrit2 = *lista_esferas[n2];
		dist2 = jugador2.Distancia(esferaCrit2.centro, NULL);

		pdatos->esfera2 = esferaCrit2;
		pdatos->raqueta2 = jugador2; 
		if (pdatos->accion2 == 1)
			OnKeyboardDown('o', 0, 0);
		if (pdatos->accion2 == -1)
			OnKeyboardDown('l', 0, 0);
	}	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	//Pulsar teclas con temporizador bot 2
	switch(key)
	{
	case 's': sprintf(buffTeclas,"s"); break;
	case 'w': sprintf(buffTeclas,"w"); break;
	case 'l': sprintf(buffTeclas,"l"); break;
	case 'o': sprintf(buffTeclas,"o"); break;
	case 'q': sprintf(buffTeclas,"q"); break;
	case 'i': sprintf(buffTeclas,"i"); break;
	case 'e': sprintf(buffTeclas,"e"); break;
	case 'p': sprintf(buffTeclas,"p"); break;
	}

	if(write(fdTeclas, buffTeclas, sizeof(buffTeclas)) == -1)
		perror("Error al escribir");
	
	if (x != 0) 
		temporizador = 0.0f;
}

void CMundo::Init()
{
	//Crear lista esferas
	lista_esferas.push_back(new Esfera);

	//Crear paredes
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;
	
	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	timer1 = timer_max;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	timer2 = timer_max;

	//Crear esferas criticas
	n = 0;
	esferaCrit = *lista_esferas[0];
	dist = jugador1.Distancia(lista_esferas[0]->centro, NULL);

	n2 = 0;
	esferaCrit2 = *lista_esferas[0];
	dist2 = jugador2.Distancia(lista_esferas[0]->centro, NULL);
	
	//Compartir memoria
	fdbot = open("/tmp/datosCompartidos.dat", O_CREAT | O_TRUNC | O_RDWR, 0666);
	write(fdbot, "hola", sizeof(DatosMemCompartida));
	pdatos = (DatosMemCompartida*)mmap((caddr_t) 0, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fdbot, 0);
	if (pdatos == MAP_FAILED) perror("Error proyectar memoria");
	close(fdbot);

	//Crear pipe coordenadas
	if(mkfifo(nombre_pipe_coord, 0666))
		perror("Error al crear el fifo \n");
	fdCoord = open(nombre_pipe_coord, O_RDONLY);
	if (fdCoord == -1)
	{
		perror("Error al abrir el fifo \n");
		terminarJuego();
	}

	//Crear pipe teclas
	if(mkfifo(nombre_pipe_teclas, 0666))
		perror("Error al crear el fifo \n");
	fdTeclas = open(nombre_pipe_teclas, O_WRONLY);
	if (fdTeclas == -1)
	{
		perror("Error al abrir el fifo \n");
		terminarJuego();
	}
}


void CMundo::terminarJuego()
{
	printf("terminarJuego\n");
	for (int i = 0; i < lista_esferas.size(); i++)
		delete lista_esferas[i];
	close(fdCoord);
	unlink(nombre_pipe_coord);
	close(fdTeclas);
	unlink(nombre_pipe_teclas);
	exit(0);
}
