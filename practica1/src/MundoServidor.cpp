//////////////////////////////////////////////////////////////////////
//                                                                  //
//                 Autor: Javier García López                       //
//                                                                  //
//////////////////////////////////////////////////////////////////////
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo(): jugador1(1), jugador2(2), timer_max(1.0f), nombre_pipe("/tmp/com"), max_esferas(10),
	nombre_pipe_coord("/tmp/coordenadas"), nombre_pipe_teclas("/tmp/teclas")
{
	Init();
}

CMundo::~CMundo()
{
	printf("Destructor\n");
	for (int i = 0; i < lista_esferas.size(); i++)
		delete lista_esferas[i];
	char out[60];
	//Cerrar pipes
	sprintf(out, "out");
	write(fdlog, out, sizeof(char) * 60);
	close(fdlog);
	close(fdCoord);
	close(fdTeclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Servidor Jugador1: %d",jugador1.puntos);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",jugador2.puntos);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for (int i = 0; i < lista_esferas.size(); i++)
		lista_esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	esfera_borrada = -1;
	//Mover los elementos
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for (int i = 0; i < lista_esferas.size(); i++)
	{
		lista_esferas[i]->Mueve(0.025f);
		if (lista_esferas[i]->reducir())
			if (lista_esferas.size() < max_esferas)
				lista_esferas.push_back(new Esfera(lista_esferas[i]->radio, 
					lista_esferas[i]->velocidad.x, -lista_esferas[i]->velocidad.y));
	}
	//Colisiones y rebotes
	int i;
	for(i=0;i<paredes.size();i++)
	{
		for (int j = 0; j < lista_esferas.size(); j++)
			paredes[i].Rebota(*lista_esferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	int n = lista_esferas.size();
	for (int i = 0; i < n; i++)
	{
		jugador1.Rebota(*lista_esferas[i]);
		jugador2.Rebota(*lista_esferas[i]);

		if(fondo_izq.Rebota(*lista_esferas[i]))
		{
			if(lista_esferas.size() == 1)
			{
				lista_esferas[i]->centro.x=0;
				lista_esferas[i]->centro.y=rand()/(float)RAND_MAX;
				lista_esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
				lista_esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
				lista_esferas[i]->radio = lista_esferas[i]->radio_max;
			}

			else
			{
				esfera_borrada = i;
				delete lista_esferas[i];
				lista_esferas.erase(lista_esferas.begin() + i);
				n = 0;
			}

			//Enviar mensaje al logger
			jugador2.puntos++;
			sprintf(buffer, "Jugador 2 marca 1 punto, lleva un total de %d puntos. \n", jugador2.puntos);
			if(write(fdlog, buffer, sizeof(buffer)) == -1)
				perror("Error al escribir");
		}

		else if(fondo_dcho.Rebota(*lista_esferas[i]))
		{
			if(lista_esferas.size() == 1)
			{
				lista_esferas[i]->centro.x=0;
				lista_esferas[i]->centro.y=rand()/(float)RAND_MAX;
				lista_esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
				lista_esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
				lista_esferas[i]->radio = lista_esferas[i]->radio_max;
			}

			else
			{
				esfera_borrada = i;
				delete lista_esferas[i];
				lista_esferas.erase(lista_esferas.begin() + i);
				n = 0;
			}

			//Enviar mensaje al logger
			jugador1.puntos++;
			sprintf(buffer, "Jugador 1 marca 1 punto, lleva un total de %d puntos. \n", jugador1.puntos);
			if(write(fdlog, buffer, sizeof(buffer)) == -1)
				perror("Error al escribir");
		}
	}
	
	for (int i = 0; i < lista_esferas.size(); i++)
		for (int j = i + 1; j < lista_esferas.size(); j++)
			lista_esferas[i]->Rebota(*lista_esferas[j]);

	if (jugador1.Impacto(jugador2.paralizante))
	{
		jugador1.paralizar = true;
		jugador2.triggerPar = false;
	}

	if (jugador2.Impacto(jugador1.paralizante))
	{
		jugador2.paralizar = true;
		jugador1.triggerPar = false;
	}

	if (jugador1.Impacto(jugador2.reductor))
	{
		jugador1.reducir = true;
		jugador1.reduct = true;
		jugador2.triggerRed = false;
	}

	if (jugador2.Impacto(jugador1.reductor))
	{
		jugador2.reducir = true;
		jugador2.reduct = true;
		jugador1.triggerRed = false;
	}

	//Efectos disparos
	if (jugador1.paralizar)
	{
		if (timer1 > 0.0f)
		{
			timer1 -= 0.025f;
			jugador1.velocidad.y = 0;
		}
		else
		{
			timer1 = timer_max;
			jugador1.paralizar = false;
		}
	}

	if (jugador2.paralizar)
	{
		if (timer2 > 0.0f)
		{
			timer2 -= 0.025f;
			jugador2.velocidad.y = 0;
		}
		else
		{
			timer2 = timer_max;
			jugador2.paralizar = false;
		}
	}

	if (jugador1.reducir)
	{
		if (jugador1.reduct)
		{
			jugador1.y1o = jugador1.y1;
			jugador1.y1 = jugador1.y2 - (jugador1.y2 + jugador1.y1) / 2;
			jugador1.reduct = false;
		}
		if (timer1 > 0.0f)
		{
			timer1 -= 0.025f;
		}
		else
		{
			timer1 = timer_max;
			jugador1.y1 = jugador1.y1o;
			jugador1.reducir = false;
		}
	}

	if (jugador2.reducir)
	{
		if (jugador2.reduct)
		{
			jugador2.y1o = jugador2.y1;
			jugador2.y1 = jugador2.y2 - (jugador2.y2 + jugador2.y1) / 2;
			jugador2.reduct = false;
		}
		if (timer2 > 0.0f)
		{
			timer2 -= 0.025f;
		}
		else
		{
			timer2 = timer_max;
			jugador2.y1 = jugador2.y1o;
			jugador2.reducir = false;
		}
	}
	
	if (fondo_izq.Impacto(jugador2.paralizante))
		jugador2.triggerPar = false;

	if (fondo_dcho.Impacto(jugador1.paralizante))
		jugador1.triggerPar = false;

	if (fondo_izq.Impacto(jugador2.reductor))
		jugador2.triggerRed = false;

	if (fondo_dcho.Impacto(jugador1.reductor))
		jugador1.triggerRed = false;
	
	sprintf(buffCoord,"%d %d", lista_esferas.size(), esfera_borrada);
	if(write(fdCoord, buffCoord, sizeof(buffCoord)) == -1)
		perror("Error al escribir");
	for (int i = 0; i < lista_esferas.size(); i++)
	{
		sprintf(buffCoord,"%f %f %f", lista_esferas[i]->centro.x, lista_esferas[i]->centro.y, lista_esferas[i]->radio);
		if(write(fdCoord, buffCoord, sizeof(buffCoord)) == -1)
			perror("Error al escribir");
	}
	sprintf(buffCoord,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d",
		jugador1.x1, jugador1.x2, jugador1.y1, jugador1.y2,
		jugador1.paralizante.centro.x, jugador1.paralizante.centro.y,
		jugador1.reductor.centro.x, jugador1.reductor.centro.y,
		jugador2.x1, jugador2.x2, jugador2.y1, jugador2.y2,
		jugador2.paralizante.centro.x, jugador2.paralizante.centro.y,
		jugador2.reductor.centro.x, jugador2.reductor.centro.y,
		jugador1.puntos, jugador2.puntos);
	if(write(fdCoord, buffCoord, sizeof(buffCoord)) == -1)
			perror("Error al escribir");

	//Terminar juego
	if (jugador1.puntos == 3 || jugador2.puntos == 3)
	{
		printf("Fin del juego\n");
		terminarJuego();
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{/*
	//Pulsar teclas con temporizador bot 2
	switch(key)
	{
	case 's':jugador1.velocidad.y = -4; break;
	case 'w':jugador1.velocidad.y = 4; break;
	case 'l':jugador2.velocidad.y = -4; break;
	case 'o':jugador2.velocidad.y = 4; break;
	case 'q':jugador1.triggerPar = true; break;
	case 'i':jugador2.triggerPar = true; break;
	case 'e':jugador1.triggerRed = true; break;
	case 'p':jugador2.triggerRed = true; break;
	}*/
}

void CMundo::Init()
{
	//Crear lista esferas
	lista_esferas.push_back(new Esfera);

	//Crear paredes
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;
	
	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	timer1 = timer_max;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	timer2 = timer_max;

	//Abrir tuberias
	fdlog = open(nombre_pipe, O_WRONLY);
	fdCoord = open(nombre_pipe_coord, O_WRONLY);
	fdTeclas = open(nombre_pipe_teclas, O_RDONLY);

	//Crear thread
	pthread_create(&threadEsperarComandos, NULL, threadComandos, this);
}


void CMundo::terminarJuego()
{
	printf("terminarJuego\n");
	for (int i = 0; i < lista_esferas.size(); i++)
		delete lista_esferas[i];
	char out[60];
	//Cerrar pipe
	sprintf(out, "out");
	write(fdlog, out, sizeof(char) * 60);
	close(fdlog);
	close(fdCoord);
	close(fdTeclas);
	exit(0);
}

void* threadComandos(void* d)
{
	CMundo* p = (CMundo*) d;
	p->recibirComandos();
}

void CMundo::recibirComandos()
{
	char buff[100];
	while(1)
	{
		usleep(10);
		if(read(fdTeclas, buff, sizeof(buff)) == -1)
			perror("Error al leer");
			//break;
		unsigned char key;
		sscanf(buff,"%c",&key);
		switch(key)
		{
		case 's':jugador1.velocidad.y = -4; break;
		case 'w':jugador1.velocidad.y = 4; break;
		case 'l':jugador2.velocidad.y = -4; break;
		case 'o':jugador2.velocidad.y = 4; break;
		case 'q':jugador1.triggerPar = true; break;
		case 'i':jugador2.triggerPar = true; break;
		case 'e':jugador1.triggerRed = true; break;
		case 'p':jugador2.triggerRed = true; break;
		}
	}
}
