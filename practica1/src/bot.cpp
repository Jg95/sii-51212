#include "DatosMemCompartida.h"
#include "Esfera.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	int fd;
	char* aux;
	DatosMemCompartida* pdatos;
	if ((fd = open("/tmp/datosCompartidos.dat", O_RDWR)) == -1)
		perror("Error al abrir el fichero");
	pdatos = (DatosMemCompartida*)mmap((caddr_t) 0, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (pdatos == MAP_FAILED)
		perror("Error al proyectar memoria");
	close(fd);
	
	while(1)
	{
		usleep(25000);
		if (((pdatos->raqueta.y2 + pdatos->raqueta.y1) / 2) > pdatos->esfera.centro.y)
			pdatos->accion = -1;
		else if (((pdatos->raqueta.y2 + pdatos->raqueta.y1) / 2) < pdatos->esfera.centro.y)
			pdatos->accion = 1;
		else
			pdatos->accion = 0;

		if (pdatos->bot2)
		{
			if (((pdatos->raqueta2.y2 + pdatos->raqueta2.y1) / 2) > pdatos->esfera2.centro.y)
				pdatos->accion2 = -1;
			else if (((pdatos->raqueta2.y2 + pdatos->raqueta2.y1) / 2) < pdatos->esfera2.centro.y)
				pdatos->accion2 = 1;
			else
				pdatos->accion2 = 0;
		}

		if (pdatos->raqueta.puntos == 3 || pdatos->raqueta2.puntos == 3)
			break;
	}
}
