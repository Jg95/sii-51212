//////////////////////////////////////////////////////////////////////
//                                                                  //
//                 Autor: Javier García López                       //
//                                                                  //
//////////////////////////////////////////////////////////////////////
// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta(int j): paralizante(0.2, 3, 0, 0, 0, 255), reductor(0.2, 3, 0, 255, 0, 0)
{
	if (j == 2)
	{
		paralizante.velocidad.x = -paralizante.velocidad.x;
		reductor.velocidad.x = -reductor.velocidad.x;
	}
	paralizar = false;
	reducir = false;
	reduct = false;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1 += velocidad.y * t;
	y2 += velocidad.y * t;
	y1o += velocidad.y * t;
	if (triggerPar == true)
		paralizante.Mueve(t);
	else
	{
		paralizante.centro.x = x1;
		paralizante.centro.y = (y2 + y1) / 2;
	}

	if (triggerRed == true)
		reductor.Mueve(t);
	else
	{
		reductor.centro.x = x1;
		reductor.centro.y = (y2 + y1) / 2;
	}
}

void Raqueta::Dibuja()
{
	Plano::Dibuja();
	paralizante.Dibuja();
	reductor.Dibuja();
}
