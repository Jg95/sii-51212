//////////////////////////////////////////////////////////////////////
//                                                                  //
//                 Autor: Javier García López                       //
//                                                                  //
//////////////////////////////////////////////////////////////////////
// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera(float r = 0.5f, float vx = 3, float vy = 3, unsigned char cr = 255, unsigned char cg = 255, unsigned char cb = 0);
	virtual ~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;
	float radio_max;

	unsigned char r;
	unsigned char g;
	unsigned char b;

	void Mueve(float t);
	void Dibuja();
	bool reducir();
	bool Rebota(Esfera& e);
};

#endif // !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
