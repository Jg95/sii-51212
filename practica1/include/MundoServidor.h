//////////////////////////////////////////////////////////////////////
//                                                                  //
//                 Autor: Javier García López                       //
//                                                                  //
//////////////////////////////////////////////////////////////////////
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <pthread.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	//Esferas y planos
	const int max_esferas;
	std::vector <Esfera *> lista_esferas;
	std::vector <Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	//Temporizador disparos
	const float timer_max;
	float timer1;
	float timer2;
	int puntos1;
	int puntos2;
	
	//Tubería logger
	const char* nombre_pipe;
	int fdlog;
	char buffer[60];

	//Pipe Coordenadas
	int esfera_borrada;
	const char* nombre_pipe_coord;
	int fdCoord;
	char buffCoord[1000];

	//Pipe Teclas
	const char* nombre_pipe_teclas;
	int fdTeclas;

	//Thread
	pthread_t threadEsperarComandos;
	void recibirComandos();

	void terminarJuego();
};

void* threadComandos(void* d);

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
