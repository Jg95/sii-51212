//////////////////////////////////////////////////////////////////////
//                                                                  //
//                 Autor: Javier García López                       //
//                                                                  //
//////////////////////////////////////////////////////////////////////
// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "Plano.h"
#include "Vector2D.h"
#include "Esfera.h"

class Raqueta : public Plano  
{
public:
	int jugador;
	Vector2D velocidad;

	Esfera paralizante;
	bool triggerPar;
	bool paralizar;
	Esfera reductor;
	bool triggerRed;
	bool reducir;
	bool reduct;
	float y1o;
	int puntos;

	Raqueta(int j = 1);
	virtual ~Raqueta();

	void Mueve(float t);

	void Dibuja();
};
